// HomeWork_18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

class Stack
{
private:
    const int block = 2;
    int size;
    int V;
    int* stack;
    //
    void add_size()
    {
        int new_size = size + block;
        int* new_stack = new int[new_size];

        for (int i = 0; i < size; i++)
        {
            *(new_stack + i) = *(stack + i); 
            //new_stack[i] = stack[i];
        }
        for (int i = size; i < new_size; i++)  new_stack[i] = 0;

        if (stack) 
        {
            delete stack;
            std::cout << "Removed stack:" << size << '\n';
        }
        stack = new_stack;
        size = new_size;
        std::cout << "Added stack:" << new_size << '\n';
    }

public:

    //
    Stack()
    {
        size = 0;
        V = -1;
        stack = nullptr;
        add_size();
    }
    ~Stack()
    {
        if (stack)
        {
            delete stack;
            size = 0;
        }
    }

    void ShowStack()
    {
        for (int i = 0; i < size; i++)
        {
            std::cout << (i == V? "(" : "") << stack[i] << (i == V ? ")" : "") << (i < size-1? ',' : ' ');
            //new_stack[i] = stack[i];
        }
        std::cout << "\nV=" << V << '\n';
    }

    void Push(int value)
    {
        if (++V >= size) add_size();
        stack[V] = value;
        ShowStack();
    }

    int Pop()
    {
        int value;
        if (V < 0)
        {
            value = -1;
            std::cout << "Pop:Stack is empty\n";
        }
        else
        {
            value = stack[V--];
            std::cout << "Pop:" << value << '\n';
        }
        ShowStack();
        return value;
    }

};

int main()
{
    Stack H;

    std::cout << "\n--- Push\n";
    H.Push(1);
    H.Push(2);
    H.Push(3);
    H.Push(4);
    H.Push(5);

    std::cout << "\n--- Pop\n";
    H.Pop();
    H.Pop();
    H.Pop();
    H.Pop();
    H.Pop();
    H.Pop();

    std::cout << "\n--- Push\n";
    H.Push(10);
    H.Push(20);

    std::cout << "\n--- Pop\n";
    H.Pop();
    H.Pop();
    H.Pop();

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
